package cartag.simov.mei.isep.ipp.pt;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.FirebaseUser;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //Map
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1000;
    private static final String[] LOCATION_PERMS = {Manifest.permission.ACCESS_FINE_LOCATION};
    private String TAG = "MainActivity";
    private NfcAdapter myNfc;

    private Activity THIS = this;
    private GoogleApiClient mApiClient;
    private TextView tvNfc;

    private boolean locationPermission = false;

    //Shared Preferences
    public static final String MY_PREFERENCES = "CarTagPreferences";
    SharedPreferences sharedPreferences;
    //Shared Preferences

    Boolean viagemIniciada;
    PendingIntent pIntent;

    private FirebaseAuth mAuth;
    Button btnLogin;

    /********************************************************************************/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        setContentView(R.layout.activity_main);

        tvNfc = findViewById(R.id.nfcStatus);
        myNfc = NfcAdapter.getDefaultAdapter(this);

        if (myNfc == null) {
            // Stop here, we definitely need NFC
            tvNfc.setText("This device doesn't support NFC.");
        } else {
            if (!myNfc.isEnabled()) {
                tvNfc.setText("NFC is disabled.");
            } else {
                tvNfc.setText("");

            }
        }

        sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        viagemIniciada = sharedPreferences.getBoolean("ViagemIniciada", false);

        //handlePermissions();

        handleIntent(getIntent());

        btnLogin = findViewById(R.id.btnLogin);
        //inicia o serviço de deteçao de atividade
        startRecognitionService();
        mAuth = FirebaseAuth.getInstance();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            btnLogin.setText("Logout");
        } else {
            btnLogin.setText("Login");
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            switch (requestCode) {
                case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                    if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Google Play Services must be installed.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    return;
            }
            if (result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned QR" + result.getContents());
                //tvMatricula.setText(result.getContents());
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        if (myNfc != null) {
            // setupForegroundDispatch(this, myNfc);
        }

        TextView tvViagemEmCurso = findViewById(R.id.tvViagemEmCurso);
        viagemIniciada = sharedPreferences.getBoolean("ViagemIniciada", false);
        if (viagemIniciada) {
            tvViagemEmCurso.setVisibility(View.VISIBLE );
        } else {
            tvViagemEmCurso.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        if (myNfc != null) {
            // stopForegroundDispatch(this, myNfc);
        }
        super.onPause();
    }

    protected void onDestroy() {
        super.onDestroy();
        if (mApiClient != null) {
            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mApiClient, pIntent);
            mApiClient.disconnect();
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent != null && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.d(TAG, "handleIntent: " + tag);
            if (rawMessages != null) {
                NdefMessage[] msg = new NdefMessage[rawMessages.length];
                for (int i = 0; i < rawMessages.length; i++) {
                    msg[i] = (NdefMessage) rawMessages[i];
                    String strMsg = new String(msg[i].getRecords()[0].getPayload());
                    strMsg = strMsg.substring(3, strMsg.length());
                    //tvMatricula.setText(strMsg);
                    Log.d(TAG, "onNewIntent: " + strMsg);
                }
                // Process the messages array.

            }
        }
    }

    public void startIniciarViagemActivity(View view) {

        if (!viagemIniciada) {
            Intent intent = new Intent(this, IniciarViagemActivity.class);
            startActivity(intent);
            startRecognitionService();

        } else {
            Toast.makeText(this, "Impossível iniciar viagem... Há uma viagem por terminar!", Toast.LENGTH_SHORT).show();
        }
    }

    public void startTerminarViagemActivity(View view) {
        Boolean viagemIniciada = sharedPreferences.getBoolean("ViagemIniciada", false);

        if (viagemIniciada) {
            Intent intent = new Intent(this, TerminarViagemActivity.class);
            startActivity(intent);

            ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(mApiClient, pIntent);
            Log.e(TAG, "A desligar o serviço de Ativity recognition, uma vez que a viagem terminou");
                    }
        else {
            Toast.makeText(this, "Impossível terminar viagem... Não há viagem iniciada!", Toast.LENGTH_SHORT).show();
        }
    }

    public void startLoginActivity(View view) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            mAuth.signOut();
            Toast.makeText(this, "Logout", Toast.LENGTH_SHORT).show();
            btnLogin.setText("Login");
        } else {
            Intent intent = new Intent(this, LoginFirebaseActivity.class);
            startActivity(intent);
        }
    }



    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Intent svcIntent = new Intent( this, ActivityRecognizedService.class );
        pIntent = PendingIntent.getService( this, 0, svcIntent, PendingIntent.FLAG_UPDATE_CURRENT );
        if (viagemIniciada) {
            ActivityRecognition.ActivityRecognitionApi.requestActivityUpdates( mApiClient, 3000, pIntent );
        }
        Log.e(TAG, "Connected to mApiClient");
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void startRecognitionService() {
        //inicia o serviço de deteçao de atividade
        mApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mApiClient.connect();
    }
}
