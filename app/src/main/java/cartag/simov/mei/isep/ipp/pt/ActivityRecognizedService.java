package cartag.simov.mei.isep.ipp.pt;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;


public class ActivityRecognizedService extends IntentService {

    //Shared Preferences
    public static final String MY_PREFERENCES = "CarTagPreferences";
    SharedPreferences sharedPreferences;
    //Shared Preferences

    int previousActivity = -100;
    int confianca = 80;


    public ActivityRecognizedService() {
        super("ActivityRecognizedService");
    }

    public ActivityRecognizedService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            handleDetectedActivities(result.getProbableActivities());
        }
        Log.e("ActivityRecogition", "onHandleIntent");
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {
        sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        Boolean viagemIniciada = sharedPreferences.getBoolean("ViagemIniciada", false);

        if (viagemIniciada) {
            for (DetectedActivity activity : probableActivities) {
                switch (activity.getType()) {
                    case DetectedActivity.IN_VEHICLE: {
                        Log.e("ActivityRecogition", "In Vehicle: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.ON_BICYCLE: {
                        Log.e("ActivityRecogition", "On Bicycle: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.RUNNING: {
                        Log.e("ActivityRecogition", "Running: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.ON_FOOT: {
                        Log.e("ActivityRecogition", "On Foot: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.STILL: {
                        Log.e("ActivityRecogition", "Still: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.TILTING: {
                        Log.e("ActivityRecogition", "Tilting: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.WALKING: {
                        Log.e("ActivityRecogition", "Walking: " + activity.getConfidence());
                        break;
                    }
                    case DetectedActivity.UNKNOWN: {
                        Log.e("ActivityRecogition", "Unknown: " + activity.getConfidence());
                        break;
                    }
                }
                if (activity.getConfidence() >= confianca) {
                    notifyUser(activity.getType());
                }
            }
        }
    }

    private void notifyUser(int activity) {
        if (previousActivity < -1) {
            previousActivity = activity;
        } else {
            if (previousActivity == DetectedActivity.IN_VEHICLE &&
                    (activity != DetectedActivity.IN_VEHICLE
                    && activity != DetectedActivity.UNKNOWN
                    && activity != DetectedActivity.ON_BICYCLE)
                    ) {
                NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                String NOTIFICATION_CHANNEL_ID = "my_channel_id_01";

                Intent intent = new Intent(this, TerminarViagemActivity.class);
                PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

                    // Configure the notification channel.
                    notificationChannel.setDescription("Channel description");
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.RED);
                    notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                    notificationChannel.enableVibration(true);
                    notificationManager.createNotificationChannel(notificationChannel);
                }


                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

                notificationBuilder.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setWhen(System.currentTimeMillis())
                        .setSmallIcon(R.drawable.cartag_logo)
                        .setTicker("CarTag")
                        //     .setPriority(Notification.PRIORITY_MAX)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText("CarTag - Terminou a viagem?"))
                        .setContentTitle("CarTag - Terminou a viagem?")
                        .setContentText("Carregue aqui para a registar.")
                        .setContentIntent(pIntent)
                        .setContentInfo("Info");


                notificationManager.notify(/*notification id*/1, notificationBuilder.build());
            }
        }

    }
}
