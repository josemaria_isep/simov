package cartag.simov.mei.isep.ipp.pt;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cartag.simov.mei.isep.ipp.pt.model.Viagem;

import static android.support.v4.content.PermissionChecker.PERMISSION_GRANTED;

public class IniciarViagemActivity  extends AppCompatActivity {

    //Map
    private static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1000;

    private static final String[] LOCATION_PERMS = {android.Manifest.permission.ACCESS_FINE_LOCATION};
    private String TAG = "MainActivity";
    private NfcAdapter myNfc;
    private TextView tvNfc;
    private EditText etMatricula;
    private EditText etKmsIniciais;
    private EditText etCC;
    private Activity THIS = this;
    private GoogleApiClient googleApiClient;
    private Location location;
    private GoogleMap googleMap;
    private Marker marker;
    private boolean locationPermission = false;
    private String userId;
    private String userEmail;
    private OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap map) {
            googleMap = map;
        }
    };
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            if (locationPermission) {
                location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            }
            if (location != null) {
                //Toast.makeText(getApplicationContext(), "Last known location:\n" + String.valueOf(location.getLatitude()) + "," + String.valueOf(location.getLongitude()), Toast.LENGTH_LONG).show();
                if (googleMap != null) {
                    LatLng place = new LatLng(location.getLatitude(), location.getLongitude());
                    googleMap.setMyLocationEnabled(locationPermission);
                    if (marker != null)
                        marker.remove();

                    marker = googleMap.addMarker(new MarkerOptions()
                            .title("Last known location")
                            .position(place));


                    CameraPosition cameraPosition = CameraPosition.builder()
                            .target(place)
                            .zoom(17)
                            .bearing(0)
                            .build();

                    // Animate the change in camera view over 2 seconds
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
                    // googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(place, 13));

                }
            }
        }

        @Override
        public void onConnectionSuspended(int i) {
            Toast.makeText(getApplicationContext(), "Connected to Google Play Services: Suspended", Toast.LENGTH_LONG).show();
        }
    };
    private GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            Toast.makeText(getApplicationContext(), "Not connected to ActivityRecognition: error code: " + connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
        }
    };

    private final int REQ_CODE_SPEECH_INPUT_MATRICULA = 100;
    private final int REQ_CODE_SPEECH_INPUT_KM = 101;
    private final int REQ_CODE_SPEECH_INPUT_CC = 102;
    private ImageButton btnSpeechMatricula;
    private ImageButton btnSpeechKm;
    private ImageButton btnSpeechCC;

    //Shared Preferences
    public static final String MY_PREFERENCES = "CarTagPreferences" ;
    SharedPreferences sharedPreferences;
    //Shared Preferences


    /**********************************************************************/


    /**
     * @param activity The corresponding {@link Activity} requesting the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);

        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        // Notice that this is the same filter as in our manifest.
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }

        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    /**
     * @param activity The corresponding {@link BaseActivity} requesting to stop the foreground dispatch.
     * @param adapter  The {@link NfcAdapter} used for the foreground dispatch.
     */
    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_viagem);

        etMatricula = findViewById(R.id.etMatricula);
        tvNfc = findViewById(R.id.nfcStatus);
        myNfc = NfcAdapter.getDefaultAdapter(this);
        etKmsIniciais = findViewById(R.id.etKmsIniciais);
        etCC = findViewById(R.id.etCentroCusto);

        if (myNfc == null) {
            // Stop here, we definitely need NFC
            tvNfc.setText("This device doesn't support NFC.");
        } else {
            if (!myNfc.isEnabled()) {
                tvNfc.setText("NFC is disabled.");
            } else {
                tvNfc.setText("");

            }
        }



        handlePermissions();

        handleIntent(getIntent());


        final Button button = findViewById(R.id.btnQrCode);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"QRCode Button clicked");
                IntentIntegrator integrator = new IntentIntegrator(THIS);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
                integrator.setPrompt("Faça scan a um QR Code");
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(true);
                integrator.setBarcodeImageEnabled(true);
                integrator.setOrientationLocked(true);
                integrator.initiateScan();

                //new IntentIntegrator(THIS).initiateScan();
            }
        });


        //Mapa
        if (isPlayServiceAvailable()) {
            buildGoogleApiClient();

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
            mapFragment.getMapAsync(onMapReadyCallback);

            googleApiClient.connect();

        } else {
            Toast.makeText(this, "Google Play Service not Available", Toast.LENGTH_LONG).show();
        }


        //Speech
        final Button btnSpeechKm = findViewById(R.id.btnSpeechKm);
        btnSpeechKm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"SpeechKM Button clicked");
                promptSpeechInput(REQ_CODE_SPEECH_INPUT_KM);
            }
        });
        final Button btnSpeechCC = findViewById(R.id.btnSpeechCC);
        btnSpeechCC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"SpeechCC Button clicked");
                promptSpeechInput(REQ_CODE_SPEECH_INPUT_CC);
            }
        });
        final Button btnSpeechMatricula = findViewById(R.id.btnSpeechMatricula);
        btnSpeechMatricula.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d(TAG,"SpeechKM Button clicked");
                promptSpeechInput(REQ_CODE_SPEECH_INPUT_MATRICULA);
            }
        });

        sharedPreferences = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        String kmsIniciais = sharedPreferences.getString("KmsFinais", "00000");
        if (!kmsIniciais.equals("00000")){
            etKmsIniciais.setText(kmsIniciais);
        }


    }



    @Override
    protected void onStop() {
        super.onStop();
        if (googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this, connectionCallbacks, connectionFailedListener)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean isPlayServiceAvailable() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(status)) {
                showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    void showErrorDialog(int code) {
        GoogleApiAvailability.getInstance().getErrorDialog(this, code, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            switch (requestCode) {
                case REQUEST_CODE_RECOVER_PLAY_SERVICES:
                    if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Google Play Services must be installed.", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    return;
            }
            if (result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {
                Log.d("MainActivity", "Scanned QR" + result.getContents());
                etMatricula.setText(result.getContents());
                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT_KM: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> speechResult = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etKmsIniciais.setText(speechResult.get(0).replace(".", "").replace(" ", ""));
                }
                break;
            }
            case REQ_CODE_SPEECH_INPUT_MATRICULA: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> speechResult = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etMatricula.setText(speechResult.get(0).replaceAll(" traço ", "-"));
                }
                break;
            }
            case REQ_CODE_SPEECH_INPUT_CC: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> speechResult = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etCC.setText(speechResult.get(0).replaceAll(" barra ", "/").replaceAll(" ponto ", "."));
                }
                break;
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        /**
         * It's important, that the activity is in the foreground (resumed). Otherwise
         * an IllegalStateException is thrown.
         */
        if (myNfc != null) {
            setupForegroundDispatch(this, myNfc);
        }


    }

    @Override
    protected void onPause() {
        /**
         * Call this before onPause, otherwise an IllegalArgumentException is thrown as well.
         */
        if (myNfc != null) {
            stopForegroundDispatch(this, myNfc);
        }
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        /**
         * This method gets called, when a new Intent gets associated with the current activity instance.
         * Instead of creating a new activity, onNewIntent will be called. For more information have a look
         * at the documentation.
         *
         * In our case this method gets called, when the user attaches a Tag to the device.
         */
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (intent != null && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            Parcelable[] rawMessages =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.d(TAG, "handleIntent: " + tag);
            if (rawMessages != null) {
                NdefMessage[] msg = new NdefMessage[rawMessages.length];
                for (int i = 0; i < rawMessages.length; i++) {
                    msg[i] = (NdefMessage) rawMessages[i];
                    String strMsg = new String(msg[i].getRecords()[0].getPayload());
                    strMsg = strMsg.substring(3, strMsg.length());
                    etMatricula.setText(strMsg);
                    Log.d(TAG, "onNewIntent: " + strMsg);
                }
                // Process the messages array.

            }
        }
    }

    // MAPS
    private boolean canAccessLocation() {
        return (hasPermission(LOCATION_PERMS[0]));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, perm));
    }

    private void handlePermissions() {
        if (!canAccessLocation()) {
            ActivityCompat.requestPermissions(this, LOCATION_PERMS, 0);
        } else {
            locationPermission = true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
            locationPermission = true;
        }
    }




    public void saveInicioViagem(View view) {
        boolean validate = true;
        String matricula = etMatricula.getText().toString();
        if (matricula.equals("")){
            Toast.makeText(this, "O campo Matrícula não pode estar em branco", Toast.LENGTH_SHORT).show();
            validate = false;
            return;
        }

        String kmsIniciais = etKmsIniciais.getText().toString();
        if (kmsIniciais.equals("")){
            Toast.makeText(this, "O campo Km's Finais não pode estar em branco", Toast.LENGTH_SHORT).show();
            validate = false;
            return;
        }


        String cc = etCC.getText().toString();
        if (cc.equals("")){
            Toast.makeText(this, "O campo Centro Custo não pode estar em branco", Toast.LENGTH_SHORT).show();
            validate = false;
            return;
        }

        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myDb = database.getReference("/");
        int kms = Integer.parseInt(kmsIniciais);
        String centroCusto = ((EditText) findViewById(R.id.etCentroCusto)).getText().toString();
        String idViagem = myDb.child("viagens").push().getKey();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            userId = user.getUid();
            userEmail = user.getEmail().replace(".", "_");
        }

        Calendar c = Calendar.getInstance();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dataInicio = sdf.format(c.getTime());

        Viagem novaViagem = new Viagem(idViagem, matricula, userId, centroCusto, dataInicio, kms);
        myDb.child(userId).child(matricula).child(idViagem).setValue(novaViagem);
        myDb.child(matricula).child(userEmail).child(idViagem).setValue(novaViagem);
        myDb.child("viagens").child(userId).child(idViagem).setValue(novaViagem);
        myDb.child("viagens-em-aberto").child(idViagem).setValue(true);


        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("ViagemIniciada", true);
        editor.putString("ViagemID", idViagem);
        editor.putString("Matricula", matricula);
        editor.putString("DataInicio", dataInicio);
        editor.putString("KmsIniciais", kmsIniciais);
        editor.commit();

        Toast.makeText(this, "Viagem iniciada e registada", Toast.LENGTH_SHORT).show();
        finish();
    }


    public void FechaInicioViagem(View view){
        finish();
    }






    /**
     * Showing google speech input dialog
     * */
    private void promptSpeechInput(int REQ_CODE) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "pt-PT");
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Fale agora");
        try {
            startActivityForResult(intent, REQ_CODE);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getApplicationContext(), "Função de Speech não suportada", Toast.LENGTH_SHORT).show();
        }
    }




}
