package cartag.simov.mei.isep.ipp.pt.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Viagem {
    private String idViagem;
    private String matricula;
    private String userID;
    private String centroCusto;
    private String dataInicio;
    private String dataFim;
    private int kmIniciais;
    private int kmFinais;

    public Viagem() {

    }

    public Viagem(String idViagem, String matricula, String userID, String centroCusto, String dataInicio, String dataFim, int kmIniciais, int kmFinais) {
        this.idViagem = idViagem;
        this.matricula = matricula;
        this.userID = userID;
        this.centroCusto = centroCusto;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.kmIniciais = kmIniciais;
        this.kmFinais = kmFinais;
    }

    public Viagem(String idViagem, String matricula, String userID, String centroCusto, String dataInicio, int kmIniciais) {
        this.idViagem = idViagem;
        this.matricula = matricula;
        this.userID = userID;
        this.centroCusto = centroCusto;
        this.dataInicio = dataInicio;
        this.kmIniciais = kmIniciais;
    }

    public String getIdViagem() {
        return idViagem;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getUserID() {
        return userID;
    }

    public String getCentroCusto() {
        return centroCusto;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public int getKmIniciais() {
        return kmIniciais;
    }

    public int getKmFinais() {
        return kmFinais;
    }


    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("idViagem", idViagem);
        result.put("matricula", matricula);
        result.put("userID", userID);
        result.put("centroCusto", centroCusto);
        result.put("dataInicio", dataInicio);
        result.put("dataFim", dataFim);
        result.put("dataFim", dataFim);
        result.put("kmIniciais", kmIniciais);
        result.put("kmFinais", kmFinais);

        return result;
    }
}
