package cartag.simov.mei.isep.ipp.pt;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import cartag.simov.mei.isep.ipp.pt.model.Viagem;

public class ListaViagens extends AppCompatActivity {

    private ListView listView;
    private ArrayList<Viagem> viagensList;
    private ArrayAdapter<Viagem> viagensAdapter;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_viagens);

        listView = findViewById(R.id.listView);

        viagensList = new ArrayList<Viagem>();

        viagensAdapter = new ArrayAdapter<Viagem>(getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1, viagensList);

        listView.setAdapter(viagensAdapter);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            userId = user.getUid();
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myDb = database.getReference("/" + userId );
        myDb.getKey();

    }
}
